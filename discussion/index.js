// discussion

let count = 0;

while(count != 5){
    console.log("while: " + count);
    count++;
}

//do while loop is guaranteed to execute at least once
let count2 = 0;
do {
    console.log("while: " + count2)
    count2 ++;
}
while( count2 < 5)

//loops

for (let count3 = 0; count3 < 10; count3++) {
    console.log("for loop: " + count3)
}

//additional topic

let myString = "Alex";
console.log(myString.length);
console.log(myString[0]);
console.log(myString[1]);
console.log(myString[2]);
console.log(myString[3]);

for (let x = 0; x < myString.length, x++){
    console.log(myString[x]);
}

// Changing of vowels using loops

let myName = "DelIZo";

for (let i = 0; i < myName.length; i++){
    if (
        myName[i].toLowerCase() == "a" ||
        myName[i].toLowerCase() == "i" ||
        myName[i].toLowerCase() == "u" ||
        myName[i].toLowerCase() == "e" ||
        myName[i].toLowerCase() == "o" 
    ){
        console.log(3);
    } else {
        console.log(myName[i]);
    }
}